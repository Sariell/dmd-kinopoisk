Feelm
===


## Technology Stack

* ArangoDB
* Python3
* Flask
* pyArango
* pytest
* Bootstrap Templates

## Installation and Running Guide

Assuming you have python3 pre-installed:

1. Clone the project
2. Open the terminal in the project directory
3. Run `pip3 install -r requirements.txt`
4. Installation is complete!
5. Run `python3 app.py`
6. Open your browser and go to http://127.0.0.1:5000/


Use case diagram
---

![Use Case Diagram Image](https://i.imgur.com/fZpZ74F.png)


Database design
---

### ER Diagram

![ER Diagram Image](https://i.imgur.com/ZaYhvEg.png)

### Justification

We decided to make User, Film and Watch list documents.
Two options existed: to make watch list an edge or to make it a vertex. Making it an edge would result in overhead when getting or deleting, so we made it a vertex.

The rating is decided to be an edge, because it represents connection between two documents. It has to store a value, but arango has such a feature.

### Hosting

Our database is hosted on Digital Ocean with WebUI on, which makes development easier for us.


Geospatial search
---

Geospatial search is implemented on users.

Two attributes were taken for geospatial search:

- Number of similar film ratings 
- Number of similar unique films in all watchlists

Distance function is defined as follows

F - total number of unique films in watchlists of target user
f - number of similar films in watchlists of another user
R - total number of ratings made by target user
r - number of similar rating made by another user

Formula:
```0.7*(F-f) + 0.3(R-r)```

It is assumed that number of similar films has bigger weight than number of similar ratings.

Appendix
---

[Project Presentation Slides](https://docs.google.com/presentation/d/1Me4-DfrOL-ey-k4Z1eK9bJ41C2Lh3DmsgShjfVPzBwE/edit#slide=id.g56a176396a_0_90)