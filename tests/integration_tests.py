from DBManager.models import User
from DBManager.models import Film


def test_create_user(db):
    user = User(nickname='test', email='test@test.ru', name='Test', surname='Testovich', password='123123')
    db.save_model(user)
    db_user = db.user_collection.get(key=user.get_key())
    assert db_user.nickname == user.nickname


def test_create_film(db):
    film = Film(country='USA', director_name="Test", genres=['Test1', 'Test2'], movie_title="Test 2", title_year=2019)
    db.save_model(film)
    db_user = db.film_collection.get(key=film.get_key())
    assert db_user.title_year == film.title_year


def test_follow(db):
    user1 = User(nickname='test1', email='test1@test.ru', name='Test1', surname='Test1', password='123123')
    user2 = User(nickname='test2', email='test2@test.ru', name='Test2', surname='Test2', password='123123')
    db.save_model(user1)
    db.save_model(user2)
    user1.follow(db.db, user2)
    follows = db.get_in_edges(user2, 'Follows')
    for follow in follows:
        if follow._to == f'Users/{user2.get_key()}':
            break
    else:
        assert False

    assert True
