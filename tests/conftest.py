import pytest

from DBManager import DBManager
from settings import DB_USER, DB_PASS, DB_URL


@pytest.fixture(scope="session")
def db(request):
    db_ = DBManager(db_url=DB_URL, db_name='Test', db_pass=DB_PASS, db_user=DB_USER, is_test=True)

    def fin():
        db_.delete_collections()
        db_.close()

    request.addfinalizer(fin)
    return db_
