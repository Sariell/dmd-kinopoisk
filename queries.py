import json
import pyArango

from DBManager import DBManager
from DBManager.models import User, Film

db = DBManager()


def get_user(nickname):
    return db.user_collection.get(key=nickname)


def get_film(film_id):
    try:
        film = db.film_collection.get(key=film_id)
        return film
    except:
        raise Exception("Not found film " + film_id)


def get_following_by_user(nickname):
    try:
        user = db.user_collection.get(key=nickname)
    except:
        raise Exception("Not found user " + nickname)
    followings = []
    for follow in db.get_out_edges(user, 'Follows'):
        try:
            to = db.user_collection.get(key=follow._to.split("/")[1]).to_dict()
        except:
            raise Exception("Not found following of user  " + nickname)
        del to['password']
        followings.append(to)
    return followings


def follow_user(user, to_follow):
    try:
        user = db.user_collection.get(key=user)
        to_follow = db.user_collection.get(key=to_follow)
    except:
        raise Exception("Not found one of users: " + user + ", " + to_follow)
    to_follow.follow(db.db, user)


def get_films_with_ratings_by_watchlist(user, watch_list_id):
    movies_and_ratings = []
    watch_list_movies = get_movies_by_watch_list(watch_list_id)  # instances of movies (id+name at least)
    for i in watch_list_movies:
        # print('Working: films & ratings')
        # film_id = i['_to'].strip('Films/')
        film_id = i['_key']
        # name = get_film_AQL(film_id)[0]['movie_title']
        # rating = get_movie_rating(user, film_id)
        rating = get_movie_rating(user, film_id)
        
        if rating == []:
            rating = "Not rated"
        else:
            rating = rating[0]['mark']

        movies_and_ratings.append({'film_id:': film_id,
                                   'film_name': i['movie_title'],
                                   'rating': rating})  # id, name, rating
    user = get_user(user)
    return {"nickname": user.nickname,
            "name": user.name,
            "surname": user.surname,
            "movies_and_ratings": movies_and_ratings}


def get_watch_lists_by_user(user):
    # aql = "FOR c IN User_Watch_list FILTER c._from == CONCAT('Users/', @user) RETURN c"
    aql = "FOR c IN 1..1 OUTBOUND CONCAT('Users/', @user) User_Watch_list RETURN c"
    bind_vars = {'user': user}
    query_result = db.db.AQLQuery(aql, rawResults=True, bindVars=bind_vars)
    result = query_result.response['result']

    wlists_with_rating = []
    for i in result:
        # i = i['_to'].strip("Watch_lists/")
        # aql = "FOR c IN Watch_lists FILTER c._key == @w_list RETURN c.rating"
        # bind_vars = {"w_list": i}
        # bind_vars = {"w_list": i['_key']}
        # query_result = db.db.AQLQuery(aql, rawResults=True, bindVars=bind_vars)
        # rating = query_result.response['result'][0]
        wlists_with_rating.append({"watch_list_id": i['_key'],
                                   "rating": i['rating']})

    user = get_user(user)
    print(user)
    return {"nickname": user.nickname,
            "name": user.name,
            "surname": user.surname,
            "wlists_with_rating": wlists_with_rating}


def get_movies_by_watch_list(id):
    # aql = "FOR c IN Watch_list_Film FILTER c._from == CONCAT('Watch_lists/', @id) RETURN c"
    aql = "FOR c IN 1..1 OUTBOUND CONCAT('Watch_lists/', @id) Watch_list_Film RETURN c"
    bind_vars = {'id': id}
    query_result = db.db.AQLQuery(aql, rawResults=True, bindVars=bind_vars)
    result = query_result.response['result']
    return result


def get_movie_rating(nickname, movie_id):
    aql = "FOR c in Ratings FILTER c._from == CONCAT('Users/', @user) && c._to == CONCAT('Films/', @movie_id) RETURN c"
    bind_vars = {'user': nickname, 'movie_id': movie_id}
    query_result = db.db.AQLQuery(aql, rawResults=True, bindVars=bind_vars)
    result = query_result.response['result']
    return result

def get_statistics():
    statistics = {}
    aql = "FOR doc IN Films COLLECT WITH COUNT INTO length RETURN length"
    query_result = db.db.AQLQuery(aql, rawResults=True)
    statistics['Films'] = query_result.response['result'][0]

    aql = "FOR doc IN Users COLLECT WITH COUNT INTO length RETURN length"
    query_result = db.db.AQLQuery(aql, rawResults=True)
    statistics['Users'] = query_result.response['result'][0]

    aql = "FOR doc IN Watch_lists COLLECT WITH COUNT INTO length RETURN length"
    query_result = db.db.AQLQuery(aql, rawResults=True)
    statistics['Watch lists'] = query_result.response['result'][0]

    aql = "FOR doc IN Ratings COLLECT WITH COUNT INTO length RETURN length"
    query_result = db.db.AQLQuery(aql, rawResults=True)
    statistics['Ratings'] = query_result.response['result'][0]

    return statistics

def num_of_people_with_film_in_watchlist(user):
    wlist = get_watch_lists_by_user(user)['wlists_with_rating']
    print("watch lists: " + str(wlist))
    user_films = []

    # get film ids
    for i in wlist:
        print("i: "+str(i))
        films = get_movies_by_watch_list(i['watch_list_id'])
        print("Films: " + str(films))
        for j in films:
            # user_films.append(j['_to'].strip('Films/'))
            user_films.append(i['_key'])
    user_films = list(set(user_films))  # to get unique elements

    # get watch_lists for each of the films
    for i in user_films:
        aql = "FOR c IN Watch_list_Film FILTER c._to == CONCAT('Films/', @film) RETURN c"
        bind_vars = {'film': i}
        query_result = db.db.AQLQuery(aql, rawResults=True, bindVars=bind_vars)
        result = query_result.response['result']
        print(result)


    return user_films


def users_geo_spatial(user_nickname, max_amount=10):
    target_user = db.db["Users"].fetchDocument(user_nickname)
    target_user_id = target_user._id

    aql_get_rated_films = \
        """ LET userRatedFilms = (FOR ur in Ratings
                FILTER ur._from == '{user}'
                RETURN ur._to)
            FOR rate in Ratings
            COLLECT film = rate._to into byFilm
            FILTER film IN userRatedFilms
            return {{film, "edges" : byFilm[*]}}"""

    ratedFilms = db.db.AQLQuery(aql_get_rated_films.format(user=target_user_id), rawResults=True, batchSize=5000)

    similarUsers = {}

    for filmRatings in ratedFilms:
        edges = filmRatings["edges"]
        target_user_mark = edges[0]["rate"]["mark"]
        for e in edges:
            rate = e["rate"]
            user_id = rate["_from"]
            if rate["mark"] == target_user_mark:
                if user_id in similarUsers.keys():
                    similarUsers[user_id] += 1
                else:
                    similarUsers[user_id] = 1

    aql_get_watchlists_films = """
    LET userWatchlists = 
        (FOR wl IN 1..1 OUTBOUND "{user_id}" User_Watch_list
        RETURN wl._id)
        
    LET userWLFilms = 
        (FOR edge IN Watch_list_Film
        FILTER edge._from IN userWatchlists
        // COLLECT film = edge._to
        RETURN DISTINCT edge._to)
    
    LET wlsWithNeededFilms = 
        (FOR edge IN Watch_list_Film
        FILTER edge._to IN userWLFilms
        RETURN DISTINCT edge._from)

    LET closeUsers =
        (FOR edge IN User_Watch_list
        FILTER edge._to IN wlsWithNeededFilms
        RETURN edge._from)
        
    LET wlsOnlyNeededFilms = (FOR edge IN Watch_list_Film
        FILTER edge._from IN wlsWithNeededFilms
        COLLECT wl = edge._from INTO edgesByWL
        //RETURN {{wl, films : (FOR wl_film IN Watch_list_Film FILTER wl_film._to IN userWLFilms RETURN DISTINCT wl_film._to)}}
        RETURN {{wl, films : (FOR wl_film IN edgesByWL[*] FILTER wl_film.edge._to IN userWLFilms RETURN DISTINCT wl_film.edge._to)}})

    FOR us_wl IN User_Watch_list
    FILTER us_wl._from IN closeUsers
    COLLECT user = us_wl._from INTO wlsByUser
    RETURN {{user, films : (FOR wl IN wlsByUser[*] RETURN (FOR filteredWl IN wlsOnlyNeededFilms FILTER filteredWl.wl == wl.us_wl._to RETURN filteredWl.films))}}
    //RETURN {{user, wls: wlsByUser[*]}}
    
    """

    user_films_list = db.db.AQLQuery(aql_get_watchlists_films.format(user_id=target_user_id), rawResults=True, batchSize=5000)

    user_films = {}

    # Aggregating films count
    for item in user_films_list:
        user = item['user']
        distinct_films = set()
        for film_list in item['films']:
            if film_list:
                for film in film_list[0]:
                    distinct_films.add(film)
        # user_films[user] = list(distinct_films)
        user_films[user] = len(distinct_films)
    

    # Stats aggregation
    user_stats = {}
    for key in user_films.keys():
        user_stats[key] = [0, user_films[key]]
    
    for key in similarUsers.keys():
        if key in user_stats.keys():
            user_stats[key][0] = similarUsers[key]
        else:
            user_stats[key] = [similarUsers[key], 0]

    # Calculating scores and finding best fit results
    target_user_stats = user_stats[target_user_id]

    user_scores = {}
    for item in user_stats.items():
        # Applying distance function
        user_scores[item[0]] = 0.3 * (target_user_stats[0] - item[1][0]) + 0.7 * (target_user_stats[1] - item[1][1])
    
    result = sorted(user_scores.items(), key=lambda item : item[1])[1:max_amount]

    # Formatting
    similar_users_results = [
        {
            "user": item[0].lstrip('Users/'),
            "user_store": db.db.fetchDocument(item[0]).getStore(),
            "score" : item[1],
            "stats": {
                "similar_ratings" : user_stats[item[0]][0],
                "similar_films" : user_stats[item[0]][1]
                }
        } for item in result
    ]
    print(result[0][1], result[len(result)-1][1])
    return similar_users_results, result[0][1], result[len(result)-1][1]


# в скольки вочлистах есть этот фильм
def popularity_of_film_in_watchlist():
    pass
