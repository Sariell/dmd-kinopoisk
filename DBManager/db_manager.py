from settings import DB_URL, DB_NAME, DB_USER, DB_PASS
from pyArango.connection import Connection
from DBManager.collections import UserCollection, FilmCollection


class DBManager:

    def __init__(self, db_url=DB_URL, db_name=DB_NAME, db_user=DB_USER, db_pass=DB_PASS, is_test=False):
        self.conn = Connection(arangoURL=db_url, username=db_user, password=db_pass)
        self.db = self.conn[db_name]
        if is_test:
            self.create_collections()
        self.user_collection = UserCollection(self.db)
        self.film_collection = FilmCollection(self.db)

    def save_model(self, model):
        model.save(self.db)

    def update_model(self, model):
        model.update(self.db)

    def create_collections(self):
        self.db.createCollection(name='Users')
        self.db.createCollection(name='Films')
        self.db.createCollection(name='Follows', className='Edges')

    def delete_collections(self):
        self.user_collection.collection.delete()
        self.film_collection.collection.delete()
        self.db['Follows'].delete()

    def close(self):
        self.conn.disconnectSession()

    def get_in_edges(self, model, edges):
        edges_ = self.db[edges]
        return model.document.getInEdges(edges_)

    def get_out_edges(self, model, edges):
        edges_ = self.db[edges]
        return model.document.getOutEdges(edges_)

    def get_edges(self, model, edges):
        edges_ = self.db[edges]
        return model.document.getEdges(edges_)
