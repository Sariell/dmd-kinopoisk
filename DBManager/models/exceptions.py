
class CollectionNotSetException(Exception):
    pass


class CollectionNotInDB(Exception):
    pass


class KeyNotSpecified(Exception):
    pass


class IncorrectFields(Exception):
    pass
