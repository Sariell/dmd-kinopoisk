from DBManager.edges import Follow
from .model import Model


class User(Model):

    fields = ['nickname', 'email', 'name', 'surname', 'password']

    def __init__(self, nickname, email, name, surname, password):
        self.nickname = nickname
        self.email = email
        self.name = name
        self.surname = surname
        self.password = password
        super().__init__(key=lambda user: user['nickname'], collection='Users')

    def to_dict(self):
        return {
            "nickname": self.nickname,
            "email": self.email,
            "name": self.name,
            "surname": self.surname,
            "password": self.password
        }

    def follow(self, db, other):
        edge = Follow(db['Follows'], self, other)
        edge.save()
        return edge
