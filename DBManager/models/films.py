from DBManager.models.exceptions import IncorrectFields
from .model import Model


class Film(Model):
    fields = ['country', 'director_name', 'genres', 'movie_title', 'title_year']

    def __init__(self, country, director_name, genres, movie_title, title_year, 
                 key=lambda film: f'{film["movie_title"].replace(" ", "-")}_{film["title_year"]}_{film["director_name"].replace(" ", "-")}'):
        self.director_name = director_name
        self.country = country
        self.genres = genres
        self.movie_title = movie_title
        self.title_year = title_year
        super().__init__(key, 'Films')

    @classmethod
    def from_dict(cls, d):
        d_store = d._store
        if not all([field in d_store for field in cls.fields]):
            return IncorrectFields(f'Incorrect fields, must be {", ".join(cls.fields)}')
        d_store = {field: d_store[field] for field in cls.fields}
        return Film(**dict(d_store))

    def to_dict(self):
        return {
            "director_name": self.director_name,
            "country": self.country,
            "genres": self.genres,
            "movie_title": self.movie_title,
            "title_year": self.title_year
        }
