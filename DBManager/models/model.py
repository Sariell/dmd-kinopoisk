from .exceptions import CollectionNotSetException, CollectionNotInDB, KeyNotSpecified, IncorrectFields


class Model:

    def __init__(self, key, collection):
        self.collection = collection
        self.key = key
        self.document = None

    def save(self, db):
        if not self.collection:
            raise CollectionNotSetException()

        collection = db[self.collection]
        self.document = collection.createDocument(self.to_dict())
        self.document._key = self.get_key()
        self.document.save()

    def update(self, db):
        if not self.collection:
            raise CollectionNotSetException()
        
        collection = db[self.collection]
        self.document = collection[self.get_key()]
        for key, value in self.to_dict().items():
            document[key] = value
        self.document.save()


    def to_dict(self):
        pass

    def get_key(self):
        if not self.key and not callable(self.key):
            raise KeyNotSpecified
        return self.key(self.to_dict())

    @classmethod
    def from_document(cls, d):
        d_store = d._store
        if not all([field in d_store for field in cls.fields]):
            return IncorrectFields(f'Incorrect fields, must be {", ".join(cls.fields)}')
        d_store = {field: d_store[field] for field in cls.fields}
        m = cls(**dict(d_store))
        m.document = d
        return m
