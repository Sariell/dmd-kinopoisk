class Collection:

    def __init__(self, db, collection_name, model):
        self.db = db
        self.collection_name = collection_name
        self.collection = self.db[collection_name]
        self.collection_model = model

    def get_name(self):
        return self.collection_name

    def filter(self):
        pass

    def get(self, key):
        model = self.collection_model.from_document(self.collection[key])
        return model


