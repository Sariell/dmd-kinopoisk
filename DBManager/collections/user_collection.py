from . import Collection
from ..models import User


class UserCollection(Collection):

    def __init__(self, db):
        super().__init__(db, "Users", User)

