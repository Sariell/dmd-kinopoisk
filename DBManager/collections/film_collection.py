from .collection import Collection
from ..models import Film


class FilmCollection(Collection):
    def __init__(self, db):
        super().__init__(db, 'Films', Film)
