

class Edge:

    def __init__(self, collection, _from, _to, _key):
        self.collection = collection
        self._from = _from
        self._to = _to
        self._key = _key

    def save(self):
        edge = self.collection.createEdge()
        edge._from = self._from.document._id
        edge._to = self._to.document._id
        edge._key = self._key
        edge.save()
