from . import Edge


class Follow(Edge):
    def __init__(self, collection, _from, _to):
        super().__init__(collection, _from, _to, f'{_from.nickname}_{_to.nickname}')
