import json

from flask import Flask, request, jsonify, session, escape, redirect, url_for, render_template, abort
from DBManager import DBManager
from DBManager.models import User, Film
import queries

app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

db = DBManager()


# index page
@app.route('/')
def index():
    if 'nickname' in session:
        return redirect(url_for('profile', nickname=session['nickname']))
    return redirect(url_for('login'))


# basic registration page
@app.route('/registration', methods=['GET', 'POST'])
def registration():
    if request.method == 'POST':
        try:
            db.user_collection.get(key=request.form['nickname'])
            db.user_collection.get(key=request.form['email'])
            abort(409)
        except:
            user = User(nickname=request.form['nickname'], email=request.form['email'], name=request.form['name'],
                        surname=request.form['surname'], password=request.form['password'])
            db.save_model(user)
            return redirect(url_for('index'))
    return render_template('auth/register.html')


# logout
@app.route('/logout')
def logout():
    session.pop('nickname', None)
    return redirect(url_for('index'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        try:
            if db.user_collection.get(key=request.form['nickname']).password == request.form['password']:
                session['nickname'] = request.form['nickname']
        except:
            abort(404)
        return redirect(url_for('index'))
    return render_template('auth/login.html')


# GET user page:
#   user info
#   link to a list of followers and following
#   follow button
#   list of watch lists
@app.route('/user/<nickname>', methods=['GET', 'POST', 'DELETE'])
def profile(nickname):
    user = db.user_collection.get(key=nickname).to_dict()
    if request.method == 'POST':
        for field in request.form:
            if request.form[field] is not '':
                user[field] = request.form[field]
        user = User(**user)
        db.update_model(user)
        return redirect(url_for('profile', nickname=nickname))
    elif request.method == 'DELETE':
        return redirect(url_for('profile', nickname=nickname))
    del user['password']
    return render_template('profile.html', user=user)


# # list of user's followers
# @app.route('/user/<nickname>/followers', methods=['GET'])
# def followers(nickname):
#     try:
#         user = db.user_collection.get(key=nickname)
#     except:
#         abort(404)
#     followers = []
#     for follow in db.get_in_edges(user, 'Follows'):
#         try:
#             fro = db.user_collection.get(key=follow._from.split("/")[1]).to_dict()
#         except:
#             abort(404)
#         del fro['password']
#         followers.append(fro)
#     return jsonify(followers)
#
#
# # list of who the user is following
# @app.route('/user/<nickname>/following', methods=['GET'])
# def following(nickname):
#     try:
#         followings = queries.get_following_by_user(nickname)
#         return jsonify(followings)
#     except:
#         abort(404)


@app.route('/user/<nickname>/watch_lists', methods=['GET'])
def user_watch_lists(nickname):
    w_lists = queries.get_watch_lists_by_user(nickname)
    return render_template('profile/watch_lists.html', data=w_lists)


# GET watch list page:
#   watch list info
#   films with their rating according to the user
@app.route('/user/<nickname>/watch_lists/<watch_list_id>', methods=['GET', 'POST', 'DELETE'])
def watch_list(nickname, watch_list_id):
    if request.method == 'GET':
        result = queries.get_films_with_ratings_by_watchlist(nickname, watch_list_id)
        return render_template('profile/watch_list_detail.html', data=result)
    elif request.method == 'POST':
        pass
    elif request.method == 'DELETE':
        pass
    return 'Watch list'


# film page:
#   film info
#   rating according to the user
#   average rating
#   the user's watch lists it is assigned to
@app.route('/film/<film_id>', methods=['GET', 'POST', 'DELETE'])
def film(film_id):
    try:
        film = queries.get_film(film_id).to_dict()
    except:
        abort(404)
    if request.method == 'GET':
        return jsonify(film)
    if request.method == 'POST':
        for field in request.form:
            if request.form[field] is not '':
                film[field] = request.form[field]
        film = Film(**film, key=lambda film: film_id)
        db.update_model(film)
        return redirect(url_for('film', film_id=film_id))
    elif request.method == 'DELETE':
        return redirect(url_for('film', film_id=film_id))

@app.route('/user/<nickname>/num_user_films')
def num_of_people_with_film_in_watchlist(nickname):
    result = queries.num_of_people_with_film_in_watchlist(nickname)
    return json.dumps(result)


@app.route('/statistics')
def statistics():
    result = queries.get_statistics()
    return  render_template('statistics.html', data=result)


@app.route('/user/<nickname>/recommendation')
def recommendation(nickname):
    result, mi, ma = queries.users_geo_spatial(nickname)
    user = queries.get_user(nickname)
    #result[0]['user'] = result[0]['user'].lstrip('Users/')
    return render_template('profile/recommendation.html',
                            data=result,
                            user=user,
                            minimal=mi,
                            maximal=ma)
# # add genre; if exists, delete genre
# @app.route('/film/<film_id>/genres', methods=['POST'])
# def film_genres(film_id):
#     if request.form['genre'] is '':
#         abort(204)
#     try:
#         film = queries.get_film(film_id).to_dict()
#     except:
#         abort(404)
#     if request.form['genre'] in film['genres']:
#         film['genres'].remove(request.form['genre'])
#     else:
#         film['genres'].append(request.form['genre'])
#     film = Film(**film, key=lambda film: film_id)
#     db.update_model(film)
#     return redirect(url_for('film', film_id=film_id))


# # all films
# @app.route('/films', methods=['GET', 'POST'])
# def films():
#     if request.method == 'POST':
#         film = Film(country=request.form['country'], director_name=request.form['director_name'], genres=[],
#                     movie_title=request.form['movie_title'], title_year=request.form['title_year'])
#         try:
#             db.save_model(film)
#         except:
#             abort(409)
#         return redirect(url_for('film', film_id=film.get_key()))
#     return 'Films'


# # add/edit/delete the user's rating of a film
# @app.route('/film/<film_id>/rate', methods=['POST', 'POST', 'DELETE'])
# def rate_film(film_id, rating):
#     return 'Rate film'


# # add/delete a film from the watch list
# @app.route('/film/<film_id>/watch_list/<watch_list_id>', methods=['POST', 'DELETE'])
# def film_watch_list(film_id, watch_list_id):
#     return 'Film watch list'


if __name__ == '__main__':
    app.run()
